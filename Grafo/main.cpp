#include <iostream>
#include "grafo/grafo.h"
#include "case/Ciudad.h"
#include "case/MatrizAdyacencia.h"

using namespace std;

int main() {
    // caso #5 , 10%
    // Implementar el grafo denominado ciudades #1 en un nuevo grafo

    // Implementar el algoritmo de deepPath que retorna el vector de INodo
    // Hacer un for que recorra el vector resultado y castee cada INodo a Ciudad e imprima el recorrido con el 
    // nombre de la ciudad 
    // una modificación muy útil esn los algoritmos de recorrido ya sea en anchura y profundidad, es que el nodo
    // tenga un puntero al nodo que lo marcó antes de meterlo en la pila o la cola, es decir, quién lo metió en la pila o la cola

    // Implementar el metodo path 
    // Calcular los siguientes P(Berna, Lisboa), P(Lisboa, Milan)
    // Hacer un for que recorra el vector resultado y castee cada INodo a Ciudad e imprima el recorrido con el 
    // nombre de la ciudad 

    // Implementar el algorithmo de Dijkstra para poder tener en el grafo
    // una tabla que me diga cuales son los caminos más cortos de todos los nodos
    // hacia todos los nodos

    // Identando el grafo.
    Grafo cities(true); 

    // Identando las ciudades.
    Ciudad lisboa = Ciudad("Lisboa", 7000000);
    Ciudad madrid = Ciudad("Madrid", 20000000);
    Ciudad barcelona = Ciudad("Barcelona", 17000000);
    Ciudad milan = Ciudad("Milan", 10000000);
    Ciudad berna = Ciudad("Berna", 5000000);
    Ciudad allCities[] = {lisboa, madrid, barcelona, milan, berna};

    // Agregandolas al grafo.
    for(int i=0; i<5; i++) {
        cities.addNode(&allCities[i]);
    }

    // Agregando arcos.
    cities.addArc(&lisboa, &barcelona, 115);
    cities.addArc(&madrid, &lisboa, 60);
    cities.addArc(&madrid, &barcelona, 88);
    cities.addArc(&barcelona, &madrid, 90);
    cities.addArc(&barcelona, &berna, 245);
    cities.addArc(&barcelona, &milan, 170);
    cities.addArc(&milan, &madrid, 230);
    cities.addArc(&berna, &milan, 115);

    // Imprimiendo counter de arcos.
    cout << "Imprimiendo counter de arcos." << endl;
    cities.printCounters();
    cout << endl;

    // Imprimiendo DeepPath
    vector<INodo*> result = cities.deepPath(&madrid);
    cout << "Imprimiendo DeepPath." << endl;
    for(int i=0; i<result.size(); i++) {
        Ciudad *city = (Ciudad*)(void*)result[i];
        cout << city->getId() << " -> " << city->getNombre() << endl;
    }
    cout << endl;

    // Imprimiendo BroadPath
    result.clear();
    result = cities.broadPath(&madrid);
    cout << "Imprimiendo BroadPath." << endl;
    for(int i=0; i<result.size(); i++) {
        Ciudad *city = (Ciudad*)(void*)result[i];
        cout << city->getId() << " -> " << city->getNombre() << endl;
    }
    cout << endl;

    // Imprimiendo Path 1
    result.clear();
    result = cities.path(&berna, &lisboa);
    cout << "Imprimiendo Path 1." << endl;
    for(int i=0; i<result.size(); i++) {
        Ciudad *city = (Ciudad*)(void*)result[i];
        cout << city->getId() << " -> " << city->getNombre() << endl;
    }
    cout << endl;

    // Imprimiendo Path 2
    result.clear();
    result = cities.path(&lisboa, &milan);
    cout << "Imprimiendo Path 2." << endl;
    for(int i=0; i<result.size(); i++) {
        Ciudad *city = (Ciudad*)(void*)result[i];
        cout << city->getId() << " -> " << city->getNombre() << endl;
    }
    cout << endl;

    // Creando la matriz de adyacencia.
    MatrixAdjacency matrix = MatrixAdjacency();
    matrix.updateMatriz(cities);

    // Mostrando caminos mas cortos de uno nodo a todos, de todos los nodos.
    cout <<endl;
    cities.dijkstra(matrix.getMatrix(), 0);
    cout <<endl;
    cities.dijkstra(matrix.getMatrix(), 1);
    cout <<endl;
    cities.dijkstra(matrix.getMatrix(), 2);
    cout <<endl;
    cities.dijkstra(matrix.getMatrix(), 3);
    cout <<endl;
    cities.dijkstra(matrix.getMatrix(), 4);
    cout <<endl;
}