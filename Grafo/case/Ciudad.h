#ifndef _CIUDAD_
#define _CIUDAD_ 1

#include <iostream>
#include "../grafo/INodo.h"

using namespace std;

/**
 * @author Francisco Javier Ovares Rojas
 */
class Ciudad : public INodo {
    public:
        /**
         * @brief Construct a new Ciudad object
         * 
         * @param pNombre 
         * @param pPoblacion 
         */
        Ciudad(string pNombre, int pPoblacion) {
            this->setId(rand()*99);
            this->poblacion = pPoblacion;
            this->nombre = pNombre;
        }

        // geters & setters.
        string getNombre() {
            return this->nombre;
        }
        void setNombre(string pNombre) {
            this->nombre = pNombre;
        }
        int getPoblacion() {
            return this->poblacion;
        }
        void setPoblacion(int pPoblacion) {
            this->poblacion = pPoblacion;
        }

    private:
        // Attributes.
        string nombre;  
        int poblacion;
};

#endif